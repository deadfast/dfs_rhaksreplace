#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_makarov_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_Makarov"};
	};
};

class CfgWeapons
{
	class Pistol;
	class Makarov : Pistol
	{
		reloadMagazineSound[] = {"\RH_aks\sound\pm_reload.wss", 0.031623, 1, 20};
		soundBegin[] = {"begin1", 1};
		begin1[] = {"\RH_aks\sound\pm.wss", 0.794328, 1, 700};
	};
	class MakarovSD : Makarov
	{
		begin1[] = {"\RH_aks\sound\pmsd.wss", 0.316228, 1, 200};
	};
};
