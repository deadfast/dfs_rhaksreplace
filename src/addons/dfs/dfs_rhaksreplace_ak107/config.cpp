#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_ak107
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_AK"};
	};
};

class CfgWeapons
{
	class AK_107_BASE;
	class AK_107_kobra : AK_107_BASE
	{
		model = "\RH_aks\RH_AK107k.p3d";
		picture = "\RH_aks\inv\AK107k.paa";
	};
	class AK_107_GL_kobra : AK_107_BASE
	{
		model = "\RH_aks\RH_AK107kgl.p3d";
		picture = "\RH_aks\inv\AK107kgl.paa";
	};
	class AK_107_GL_pso : AK_107_GL_kobra
	{
		model = "\RH_aks\RH_AK107glsp.p3d";
		picture = "\RH_aks\inv\AK107glsp.paa";
	};
	class AK_107_pso : AK_107_BASE
	{
		model = "\RH_aks\RH_AK107sp.p3d";
		picture = "\RH_aks\inv\AK107sp.paa";
	};
};
