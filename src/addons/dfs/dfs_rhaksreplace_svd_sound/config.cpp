#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_svd_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_e", "CAWeapons"};
	};
};

class CfgWeapons
{
	class Rifle;
	class SVD : Rifle
	{
		soundBegin[] = {"begin1", 1};
		begin1[] = {"\RH_aks\Sound\SVD.wss", 1.77828, 1, 1200};
		reloadMagazineSound[] = {"\RH_aks\sound\svd_reload.wss", 0.056234, 1, 20};
	};
};
