#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_akm
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AK_BASE;
	class AK_47_M : AK_BASE
	{
		model = "\RH_aks\RH_akm.p3d";
		picture = "\RH_aks\inv\akm.paa";
	};
	class AK_47_S : AK_47_M
	{
		model = "\RH_aks\RH_aks47b.p3d";
		picture = "\RH_aks\inv\aks47b.paa";
	};
	class AKS_GOLD : AK_47_S
	{
		model = "\RH_aks\RH_aks47g.p3d";
		picture = "\RH_aks\inv\aks47g.paa";
	};
};

class CfgMagazines
{
	class CA_Magazine;
	class 30Rnd_762x39_AK47 : CA_Magazine
	{
		model = "\RH_aks\mags\mag_ak47.p3d";
		picture = "\RH_aks\inv\m_ak.paa";
	};
};
