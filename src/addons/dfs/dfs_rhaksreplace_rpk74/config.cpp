#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_rpk74
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AK_74;
	class RPK_74 : AK_74
	{
		model = "\RH_aks\RH_rpk74.p3d";
		picture = "\RH_aks\inv\rpk74.paa";
	};
};

class CfgMagazines
{
	class CA_Magazine;
	class 30Rnd_545x39_AK : CA_Magazine
	{
		model = "\RH_aks\mags\mag_aksu.p3d";
	};
	class 30Rnd_545x39_AKSD : 30Rnd_545x39_AK
	{
	};
};
