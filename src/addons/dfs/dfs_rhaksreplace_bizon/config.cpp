#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_bizon
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_bizon"};
	};
};

class CfgWeapons
{
	class Rifle;
	class bizon : Rifle
	{
		model = "\rh_aks\RH_bizon.p3d";
		picture = "\rh_aks\inv\bizon.paa";
	};
	class bizon_silenced : bizon
	{
		model = "\rh_aks\RH_bizonsd.p3d";
		picture = "\rh_aks\inv\bizonsd.paa";
	};
};
