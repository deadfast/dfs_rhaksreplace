#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_aks74u_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AK_BASE;
	class AKS_BASE : AK_BASE
	{
		class Single;
		class Burst;
		class FullAuto;
	};
	class AKS_74_U : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74u.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74u.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74u.wss", 1.77828, 1, 1000};
		};
	};
	class AKS_74_UN_kobra : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\aks74u_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74usd.wss", 1.77828, 1, 50};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74usd.wss", 1.77828, 1, 50};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\aks74usd.wss", 1.77828, 1, 50};
		};
	};
};
