#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_aks74
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AKS_BASE;
	class AKS_74 : AKS_BASE
	{
		model = "\RH_aks\RH_aks74.p3d";
		picture = "\RH_aks\inv\aks74.paa";
	};
	class AKS_74_kobra : AKS_BASE
	{
		model = "\RH_aks\RH_aks74k.p3d";
		picture = "\RH_aks\inv\aks74k.paa";
	};
	class AKS_74_pso : AKS_BASE
	{
		model = "\RH_aks\RH_aks74sp.p3d";
		picture = "\RH_aks\inv\aks74sp.paa";
	};
};

class CfgMagazines
{
	class CA_Magazine;
	class 30Rnd_545x39_AK : CA_Magazine
	{
		model = "\RH_aks\mags\mag_aksu.p3d";
	};
	class 30Rnd_545x39_AKSD : 30Rnd_545x39_AK
	{
	};
};
