#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_rpk74_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class Mode_FullAuto;
class Mode_SemiAuto;

class CfgWeapons
{
	class AK_74;
	class RPK_74 : AK_74
	{
		reloadMagazineSound[] = {"\RH_aks\sound\rpk74_reload.wss", 0.056234, 1, 20};
		
		class FullAuto : Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\rpk74.wss", 1.77828, 1, 1000};
		};
		class manual : Mode_FullAuto
		{
			begin1[] = {"\RH_aks\Sound\rpk74.wss", 1.77828, 1, 1000};
		};
		class Single : Mode_SemiAuto
		{
			begin1[] = {"\RH_aks\Sound\rpk74.wss", 1.77828, 1, 1000};
		};
	};
};
