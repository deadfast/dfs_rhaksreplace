#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_bizon_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_bizon"};
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;

class CfgWeapons
{
	class Rifle;
	class bizon : Rifle
	{
		reloadMagazineSound[] = {"\rh_aks\sound\bizon_Reload.wss", 0.056234, 1, 20};
		
		class Single : Mode_SemiAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\bizon.wss", 1.77828, 1, 1000};
		};
		class Burst : Mode_Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\bizon.wss", 1.77828, 1, 1000};
		};
		class FullAuto : Mode_FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\bizon.wss", 1.77828, 1, 1000};
		};
	};
	class bizon_silenced : bizon
	{
		class Single : Single
		{
			begin1[] = {"\RH_aks\sound\bizon_sd.wss", 0.562341, 1, 50};
		};
		class Burst : Burst
		{
			begin1[] = {"\RH_aks\sound\bizon_sd.wss", 0.562341, 1, 50};
		};
		class FullAuto : FullAuto
		{
			begin1[] = {"\RH_aks\sound\bizon_sd.wss", 0.562341, 1, 50};
		};
	};
};
