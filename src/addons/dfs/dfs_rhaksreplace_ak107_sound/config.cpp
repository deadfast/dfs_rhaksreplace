#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_ak107_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_AK"};
	};
};

class CfgWeapons
{
	class AK_BASE;
	class AK_107_BASE : AK_BASE
	{
		class Single;
		class Burst;
		class FullAuto;
		class GP25Muzzle;
	};
	class AK_107_kobra : AK_107_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AK_107_GL_kobra : AK_107_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		
		class GP25Muzzle : GP25Muzzle
		{
			sound[] = {"\RH_aks\Sound\GP30.wss", 0.562341, 1};
			reloadMagazineSound[] = {"\RH_aks\Sound\GP30_reload.wss", 0.056234, 1, 20};
		};
	};
	class AK_107_GL_pso : AK_107_GL_kobra
	{
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AK_107_pso : AK_107_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
};
