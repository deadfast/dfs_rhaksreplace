#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_aks74_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AK_BASE;
	class AKS_BASE : AK_BASE
	{
		class Single;
		class Burst;
		class FullAuto;
		class GP25Muzzle;
	};
	class AKS_74 : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AKS_74_kobra : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AKS_74_pso : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AKS_74_NSPU : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
	class AKS_74_GOSHAWK : AKS_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\ak74_reload.wss", 0.056234, 1, 20};
		
		class Single : Single
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class Burst : Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
		class FullAuto : FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\Sound\ak74.wss", 1.77828, 1, 1000};
		};
	};
};
