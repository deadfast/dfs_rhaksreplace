#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_akm_sound
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;

class CfgWeapons
{
	class AK_BASE;
	class AK_47_M : AK_BASE
	{
		reloadMagazineSound[] = {"\RH_aks\sound\Ak47_reload.wss", 0.056234, 1, 20};
		
		class Single : Mode_SemiAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\ak47.wss", 1.77828, 1, 1000};
		};
		class Burst : Mode_Burst
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\ak47.wss", 1.77828, 1, 1000};
		};
		class FullAuto : Mode_FullAuto
		{
			soundbegin[] = {"begin1", 1.0};
			begin1[] = {"\RH_aks\sound\ak47.wss", 1.77828, 1, 1000};
		};
	};
};
