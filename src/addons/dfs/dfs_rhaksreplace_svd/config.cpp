#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_svd
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_e", "CAWeapons"};
	};
};

class CfgWeapons
{
	class Rifle;
	class SVD : Rifle
	{
		model = "\RH_aks\RH_svd.p3d";
		picture = "\RH_aks\inv\svd.paa";
	};
};
