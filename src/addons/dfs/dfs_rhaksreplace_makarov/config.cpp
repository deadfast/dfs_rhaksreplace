#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_makarov
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_Makarov"};
	};
};

class CfgWeapons
{
	class Pistol;
	class Makarov : Pistol
	{
		model = "\RH_aks\RH_pm.p3d";
		picture = "\RH_aks\inv\pm.paa";
	};
	class MakarovSD : Makarov
	{
		model = "\RH_aks\RH_pmsd.p3d";
		picture = "\RH_aks\inv\pmsd.paa";
	};
};
