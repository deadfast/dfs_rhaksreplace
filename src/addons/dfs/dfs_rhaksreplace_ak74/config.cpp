#define private		0
#define protected	1
#define public		2

class CfgPatches
{
	class DFS_rhAksReplace_ak74
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.50;
		requiredAddons[] = {"RH_aks_cfg", "CAWeapons_E_AK"};
	};
};

class CfgWeapons
{
	class AK_BASE;
	class AK_74 : AK_BASE
	{
		model = "\RH_aks\RH_ak74.p3d";
		picture = "\RH_aks\inv\ak74.paa";
	};
	class AK_74_GL : AK_BASE
	{
		model = "\RH_aks\RH_ak74gl.p3d";
		picture = "\RH_aks\inv\ak74gl.paa";
	};
	class AK_74_GL_kobra : AK_74_GL
	{
		model = "\RH_aks\RH_ak74kgl.p3d";
		picture = "\RH_aks\inv\ak74kgl.paa";
	};
};

class CfgMagazines
{
	class CA_Magazine;
	class 30Rnd_545x39_AK : CA_Magazine
	{
		model = "\RH_aks\mags\mag_aksu.p3d";
	};
	class 30Rnd_545x39_AKSD : 30Rnd_545x39_AK
	{
	};
};
